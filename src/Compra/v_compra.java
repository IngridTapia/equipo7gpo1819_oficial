
package Compra;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class v_compra {
     public v_compra(){
        marco();
}
     public void marco(){
        JFrame f= new JFrame ();
        f.setSize(500,300);
        f.setLocation(100,100);
        f.setTitle("COMPRAS");

       
       JPanel panelMayor=new JPanel(new BorderLayout());
       
       JPanel panelNorte= new JPanel();
       JPanel panelCentro= new JPanel();
       JPanel panelSur= new JPanel();
       JPanel panelEste= new JPanel();
       JPanel panelOeste= new JPanel();
       
             
       GridBagLayout gbl1=new GridBagLayout();
       GridBagConstraints gbc1 = new GridBagConstraints();
       panelCentro.setLayout(gbl1);
       
             
       GridLayout gl2=new GridLayout(1,6);
       panelSur.setLayout(gl2);
       
       JLabel lblTitulo=new JLabel("AGREGAR CANTIDAD A PRODUCTOS");
 

       String[] lista={"HOMBRE","MUJER","SODAS","DULCES","ROPA","HIGIENE INTIMA"};
       JLabel lbl5=new JLabel("\t\nLINEA:");
       JComboBox cb1=new JComboBox(lista);
       
       JLabel lbl1=new JLabel("\t\nCODIGO:");
       JTextField tf1= new JTextField(30);
       JLabel lbl2=new JLabel("\t\nNOMBRE:");
       JTextField tf2= new JTextField(30);
       JLabel lbl3=new JLabel("\t\nCANTIDAD A AGREGAR:");
       JTextField tf3= new JTextField(30);
       JLabel lbl4=new JLabel("\t\nFECHA DE CADUCIDAD");
        
       String[] lista1={"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20",
      "21","22","23","24","25","26","27","28","29","30","31"};
      JComboBox cb2=new JComboBox(lista1);
      
      String[] lista2={"ENERO","FEREBRO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
       JComboBox cb3=new JComboBox(lista2);
      
       String[] lista3={"1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008",
       "2009","2010","2011","2012","2013","2014","2015","2016"};
       JComboBox cb4=new JComboBox(lista3);
       
       JLabel lbl6=new JLabel("\t\nDIA:");
       JLabel lbl7=new JLabel("\t\nMES:");
       JLabel lbl8=new JLabel("\t\nAÑO:");

       //componentes para panel sur
       JButton bs1=new JButton();
       JButton bs2=new JButton();
       JButton bs3=new JButton();
       
       
       bs1.setText("INVENTARIO");
       bs2.setText("AGREGAR CANTIDAD");
       bs3.setText("CANCELAR");
      

       panelNorte.add(lblTitulo);
       gbc1.anchor=GridBagConstraints.WEST;
       
       panelCentro.add(lbl5,gbc1);
       panelCentro.add(cb1,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=11;
       
       panelCentro.add(lbl1,gbc1);
       panelCentro.add(tf1,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=22;
       
       panelCentro.add(lbl2,gbc1);
       panelCentro.add(tf2,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=33;
        
       panelCentro.add(lbl3,gbc1);
       panelCentro.add(tf3,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=44;
        
       panelCentro.add(lbl4,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=55;
        
       panelCentro.add(lbl6,gbc1);
       panelCentro.add(cb2,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=66;
        
       panelCentro.add(lbl7,gbc1);
       panelCentro.add(cb3,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=77;
       
       panelCentro.add(lbl8,gbc1);
       panelCentro.add(cb4,gbc1);
       gbc1.gridwidth=1;
       gbc1.gridy=88;
  
       panelSur.add(bs1);
       panelSur.add(bs2);
       panelSur.add(bs3);
 
      panelMayor.add(panelNorte, BorderLayout.NORTH);
      panelMayor.add(panelCentro, BorderLayout.CENTER);
      panelMayor.add(panelSur, BorderLayout.SOUTH);
      
      f.add(panelMayor);
      f.setVisible(true);
     }
}
    